package com.example.shittinkitten.hoofbook;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.EditText;
import android.widget.TextView;
import android.graphics.Color;

public class CreateAccountActivity extends AppCompatActivity
{

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.activity_create_account);

		// Layout object creation
		RelativeLayout CreateAccountLayout =  new RelativeLayout(this);

		// Setting object background
//	CreateAccountLayout.setBackgroundResource(R.drawable.ic_launcher);

		//
		RelativeLayout.LayoutParams CreateAccountBannerDetails = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT);
		CreateAccountBannerDetails.addRule(RelativeLayout.CENTER_HORIZONTAL);
		CreateAccountBannerDetails.addRule(RelativeLayout.ALIGN_TOP);

		// Create Account Banner
		TextView CreateAccountBanner = new TextView(this);
		CreateAccountBanner.setId(4);
		CreateAccountBanner.setText("Create Account");
		CreateAccountBanner.setTextSize(32);

		// Positioning sign in button
		RelativeLayout.LayoutParams SignInButtonDetails = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT);
		SignInButtonDetails.addRule(RelativeLayout.CENTER_HORIZONTAL);
		SignInButtonDetails.addRule(RelativeLayout.CENTER_VERTICAL);

		// Sign in button object creation
		Button SignInButton = new Button(this);
		SignInButton.setText(R.string.SignInButtonText);
		SignInButton.setBackgroundColor(Color.CYAN);
		SignInButton.setId(1);

		// username input
		EditText UserNameInput = new EditText(this);
		UserNameInput.setId(3);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_create_account, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		//noinspection SimplifiableIfStatement
		if (id == R.id.action_settings)
		{
			return true;
		}

		return super.onOptionsItemSelected(item);
	}
}
