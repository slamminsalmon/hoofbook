package com.example.shittinkitten.hoofbook;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RelativeLayout;
import android.widget.Button;
import android.graphics.Color;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity
{

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		/* The following line is commented out while I a learning how to create user interfaces via
		java instead of xml
		setContentView(R.layout.activity_main);
		*/

		// Layout object creation
		RelativeLayout myLayout =  new RelativeLayout(this);

		// Setting object background
		myLayout.setBackgroundResource(R.drawable.ic_launcher);

		// Positioning sign in button
		RelativeLayout.LayoutParams SignInButtonDetails = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT);
		SignInButtonDetails.addRule(RelativeLayout.CENTER_HORIZONTAL);
		SignInButtonDetails.addRule(RelativeLayout.CENTER_VERTICAL);

			// Sign in button object creation
			Button SignInButton = new Button(this);
			SignInButton.setText(R.string.SignInButtonText);
			SignInButton.setBackgroundColor(Color.CYAN);
			SignInButton.setId(1);

		// Positioning create account button
		RelativeLayout.LayoutParams CreateAccountButtonDetails = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT);
		CreateAccountButtonDetails.addRule(RelativeLayout.BELOW, SignInButton.getId());
		CreateAccountButtonDetails.addRule(RelativeLayout.CENTER_HORIZONTAL);
		CreateAccountButtonDetails.setMargins(0,25,0,0);
		CreateAccountButtonDetails.addRule(RelativeLayout.CENTER_VERTICAL);

			// Create Account button object creation
			Button CreateAccountButton = new Button(this);
			CreateAccountButton.setText(R.string.CreateAccountButtonText);
			CreateAccountButton.setBackgroundColor(Color.GRAY);
			CreateAccountButton.setId(2);


		// Positioning create username field
		RelativeLayout.LayoutParams UserNameDetails = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT);

		// Welcome Banner
		TextView WelcomeBannerText = new TextView(this);
		WelcomeBannerText.setId(4);
		RelativeLayout.LayoutParams WelcomeTextDetails = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT);
		WelcomeTextDetails.addRule(RelativeLayout.CENTER_HORIZONTAL);
		WelcomeTextDetails.addRule(RelativeLayout.ALIGN_TOP);
		WelcomeBannerText.setText("Welcome to Hoof Book!");
		WelcomeBannerText.setTextSize(32);

		// Adding button 'widget' to layout object (button is a child of layout)
				myLayout.addView(SignInButton, SignInButtonDetails);
		myLayout.addView(CreateAccountButton, CreateAccountButtonDetails);
		myLayout.addView(WelcomeBannerText, WelcomeTextDetails);

		// Set this activities content/display to this view
		setContentView(myLayout);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		//noinspection SimplifiableIfStatement
		if (id == R.id.action_settings)
		{
			return true;
		}

		return super.onOptionsItemSelected(item);
	}
}
